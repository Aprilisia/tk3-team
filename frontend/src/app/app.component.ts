import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Tugas Kelompok 3';
  username = localStorage.getItem("username");
  role = localStorage.getItem("role");
  isLoggedIn = localStorage.getItem("username") && localStorage.getItem("role") ? true : false;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    if(!this.isLoggedIn) 
      this.router.navigateByUrl("/login")
  }

  logout() {
    localStorage.clear()
    window.location.href = "/login";
  }
}
