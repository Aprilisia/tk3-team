import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  username = localStorage.getItem("username");
  role = localStorage.getItem("role");
  isLoggedIn = localStorage.getItem("username") && localStorage.getItem("role") ? true : false;
  
  id: number | any;
  user: any;
  form: FormGroup | any;
  filedata: any;
  gambar: any

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    if(this.isLoggedIn && this.role != "Pembeli") {

      this.id = this.route.snapshot.params['idUser'] ?? "";
      
      this.form = this.formBuilder.group({
        nama: new FormControl("", [ Validators.required ]),
        role:  new FormControl("", [ Validators.required ]),
        jenis_kelamin: new FormControl(''),
        alamat: new FormControl(''),
        file_ktp: new FormControl(''),
        tempat_lahir: new FormControl(''),
        tgl_lahir: new FormControl(''),
        username: new FormControl('', [ Validators.required ]),
        password: new FormControl(''),
        confirm_password: new FormControl(''),
      });
  
      if(this.id) {
        this.http.get("http://127.0.0.1:8000/api/user/" + this.id)
        .subscribe(x => this.form.patchValue(x))
      }
    } else {
      if(this.role == "Pembeli")
        this.router.navigateByUrl('/')
      else
        this.router.navigateByUrl('/login')
    }
  }

  get f(){
    return this.form.controls;
  }

  onFileChange(e:any){
    this.form.patchValue({
      gambar: e.target.files[0]
    })
  }

  submit(){
    if(this.form.value.password !== this.form.value.confirm_password) {
      alert("Password harus sama dengan konfirmasi password");
      return;
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    var formData = new FormData();
    formData.append('nama', this.form.value.nama);
    formData.append('role', this.form.value.role);
    formData.append('jenis_kelamin', this.form.value.jenis_kelamin ?? "");
    formData.append('alamat', this.form.value.alamat ?? "");
    formData.append('tempat_lahir', this.form.value.tempat_lahir ?? "");
    formData.append('tgl_lahir', this.form.value.tgl_lahir ?? "");
    formData.append('username', this.form.value.username);
    formData.append('password', this.form.value.password);
    formData.append('file_ktp', this.form.controls['file_ktp'].value ?? "");
    
    this.http.post("http://127.0.0.1:8000/api/user/" + this.id ?? "", formData, httpOptions)
      .subscribe(data => {
        alert("Success")
        this.router.navigateByUrl('user/index');
      }); 
  }
}
