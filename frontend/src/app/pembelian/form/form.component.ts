import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  id_user = localStorage.getItem("id_user");
  username = localStorage.getItem("username");
  role = localStorage.getItem("role");
  isLoggedIn = localStorage.getItem("username") && localStorage.getItem("role") ? true : false;

  id: number | any;
  pembelian: any;
  barang: any;
  form: FormGroup | any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    if(this.isLoggedIn && this.role != "Admin") {
      this.id = this.route.snapshot.params['idPembelian'] ?? "";

      this.http.get("http://127.0.0.1:8000/api/barang/")
      .subscribe(data => {
        this.barang = data;
      })
      
      this.form = this.formBuilder.group({
        barang:  new FormControl("", [ Validators.required ]),
        validasi:  new FormControl(""),
        total:  new FormControl(""),
        created_by:  new FormControl(this.id_user, [ Validators.required ]),
      });
  
      if(this.id) {
        this.http.get("http://127.0.0.1:8000/api/pembelian/" + this.id)
        .subscribe(x => this.form.patchValue(x))
      }
    } else {
      if(this.role == "Admin")
        this.router.navigateByUrl('/')
      else
        this.router.navigateByUrl('/login')
    }
  }

  get f(){
    return this.form.controls;
  }

  submit(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    var formData = new FormData();
    formData.append('barang', this.form.value.barang);
    formData.append('is_validasi', this.form.value.validasi);
    formData.append('total', this.form.value.total);
    formData.append('role', this.role ?? "null");
    formData.append('created_by', this.form.value.created_by);
    
    this.http.post("http://127.0.0.1:8000/api/pembelian/" + this.id ?? "", formData, httpOptions)
      .subscribe(data => {
        alert("Success")
        this.router.navigateByUrl('pembelian/index');
      }); 
  }
}