import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { FormComponent } from './form/form.component';

const routes: Routes = [
  { path: "barang", redirectTo: "barang/index", pathMatch: "full" },
  { path: "barang/index", component: IndexComponent },
  { path: "barang/create", component: FormComponent },
  { path: "barang/edit/:idBarang", component: FormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BarangRoutingModule { }
