import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BarangRoutingModule } from './barang-routing.module';
import { IndexComponent } from './index/index.component';
import { FormComponent } from './form/form.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    IndexComponent,
    FormComponent
  ],
  imports: [
    CommonModule,
    BarangRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class BarangModule { }
