import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  form: FormGroup | any;
  contents: any;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {    
    this.form = this.formBuilder.group({
      username:  new FormControl("", [ Validators.required ]),
      password:  new FormControl("", [ Validators.required ]),
    });
  }

  get f(){
    return this.form.controls;
  }

  submit(){
    var formData = new FormData();
    formData.append('username', this.form.value.username);
    formData.append('password', this.form.value.password);
    
    fetch(
      `http://127.0.0.1:8000/api/login/`,
      {
        method: "POST",
        headers: {
          'Accept': 'application/json'
        },
        body: formData
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        alert(data.message);
        if(data.status === 200) {
          let response = data.data
          localStorage.setItem('id_user', response.id)
          localStorage.setItem('role', response.role)
          localStorage.setItem('username', response.username)
          window.location.href = "/";
        }
      })
      .catch((err) => {
        alert(err.message);
        console.log(err);
      });
    }
  }
